void main(List<String> arguments) {
  final String address = 'Suumbaeva st., Bishkek';
  print(address.length);

  int age = 20;
  double weight = 63.2;
  String name = 'Daniyar';
  print('Возраст: $age, вес: $weight, имя: $name');

  int a = 20;
  String b = '241292';
  a = int.parse(b);
  print(a);

  var list = [1, 7, 12, 3, 56, 2, 87, 34, 54];
  int f = list.first;
  int fi = list.elementAt(4);
  int l = list.last;
  print('$f, $fi, $l');

  var list1 = [3, 12, 43, 1, 25, 6, 5, 7];
  var list2 = [55, 11, 23, 56, 78, 1, 9];
  var newList = List.from(list1)..addAll(list2);
  print(newList);

  var list3 = [
    'a',
    'd',
    'F',
    'l',
    'u',
    't',
    't',
    'e',
    'R',
    'y',
    '3',
    'b',
    'h',
    'j'
  ];
  var list4 = list3.removeRange(0, 2);
  var list5 = list3.removeRange(7, 12);
  print(list3);
  
}
