void main(List<String> args) {
  int fingerNumber = 1;

  if (fingerNumber == 1) {
    print('большой палец');
  } else if (fingerNumber == 2) {
    print('second finger');
  } else if (fingerNumber == 3) {
    print('third finger');
  } else if (fingerNumber == 4) {
    print('fourth finger');
  } else if (fingerNumber == 5) {
    print('fifth finger');
  } else {
    print('Invalid number');
  }

  int min = 0;
  if (min >= 0 && min <= 15) {
    print('first quarter');
  } else if (min > 15 && min <= 30) {
    print('second quarter');
  } else if (min > 30 && min <= 45) {
    print('third quarter');
  } else if (min > 45 && min <= 59) {
    print('fourth quarter');
  } else if (min > 59 && min < 0) {
    print('Invalid value');
  }

  String lang = 'en';
  List<String> arr = [];
  if (lang == 'en') {
    print(arr = [
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
      'Sunday'
    ]);
  } else if (lang == 'ru') {
    print(arr = [
      'Понедельниик',
      'Вторник',
      'Среда',
      'Четверг',
      'Пятница',
      'Суббота',
      'Воскресенье'
    ]);
  }

  String line = 'abcde';
  var line1 = line.substring(0, 1);
  if (line1 == 'a') {
    print('yes');
  } else {
    print('no');
  }

  int num = 1;
  switch (num) {
    case 1:
      {
        print('winter');
      }
      break;
    case 2:
      {
        print('summer');
      }
      break;
    case 3:
      {
        print('spring');
      }
      break;
    case 4:
      {
        print('autumn');
      }
      break;
    default:
      {
        print('Invalid choice');
      }
      break;
  }

  int a = 0;
  if (a < 0) {
    print('right');
  } else {
    print('not true');
  }

  String numbers = '333621';
  var num1 = (numbers.substring(3, 6));
  var num2 = (numbers.substring(0, 3));
  int sum1 = int.parse(num1);
  int sum2 = int.parse(num2);
  int sum3 = (sum1 ~/ 100);
  int sum4 = (sum1 % 100) ~/ 10;
  int sum5 = ((sum1 % 100) % 10);
  int allSum1 = (sum3 + sum4 + sum5);
  int sum6 = (sum2 ~/ 100);
  int sum7 = (sum2 % 100) ~/ 10;
  int sum8 = ((sum2 % 100) % 10);
  int allSum2 = (sum8 + sum7 + sum6);
  if (allSum1 == allSum2) {
    print('yes');
  } else {
    print('no');
  }

  String trafLight = 'red';
  if (trafLight == 'red') {
    print('stay');
  } else if (trafLight == 'yellow') {
    print('get ready');
  } else {
    print('pass');
  }
}
