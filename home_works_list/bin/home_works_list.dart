void main(List<String> arguments) {
  var list = [1, 7, 12, 3, 56, 2, 87, 34, 54];
  print('${list.first}, ${list.elementAt(4)}, ${list.last}');

  var list1 = [3, 12, 43, 1, 25, 6, 5, 7];
  var list2 = [55, 11, 23, 56, 78, 1, 9];
  var newList = List.from(list1)..addAll(list2);
  print(newList);

  var list3 = [
    'a',
    'd',
    'F',
    'l',
    'u',
    't',
    't',
    'e',
    'R',
    'y',
    '3',
    'b',
    'h',
    'j'
  ];
  print(list3.sublist(2, 9));

  List list6 = [1, 2, 3, 4, 5, 6, 7];
  print('${list6.contains(3)}, ${list6.first}, ${list6.last}');

  List list7 = [601, 123, 2, "dart", 45, 95, "dart24", 1];
  print('${list7.contains("dart")}, ${list7.contains(951)}');

  List list8 = ['post', 1, 0, 'flutter'];
  String myDart = 'Flutter';
  print(list8.contains(myDart.toLowerCase()));

  List<String> list9 = ["I", "Started", "Learn", "Flutter", "Since", "April"];
  String myFlutter = "";
  myFlutter = list9.join(" * ").toString();
  print(myFlutter);

  List list10 = [1, 9, 3, 195, 202, 2, 5, 7, 9, 10, 3, 15, 0, 11];
  list10.sort((a, b) => a.compareTo(b));
  print(list10);
}
